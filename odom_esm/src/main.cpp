/**
 * @author André Nishitani <atoshio25@gmail.com>
 * @date 2014.06.02
 * @section DESCRITPION
 */

#include <cstdio>
#include <fstream>

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <esm.h>

cv::Mat get_mono_image(char *format, int i) {
	char filename[256];
	sprintf(filename, format, i);
	return cv::imread(filename, CV_LOAD_IMAGE_GRAYSCALE);
}

void write_to_file(std::string filename, cv::Mat matrix) {
	FILE *file;
	file = fopen(filename.c_str(), "w");
	for (int i = 0; i < matrix.rows; i++) {
		for (int j = 0; j < matrix.cols; j++) {
			fprintf(file, "%f ", matrix.at<float>(i, j));
		}
		fprintf(file, "\n");
	}
	fclose(file);
}

int main(int argc, char **argv) {
	cv::Mat list;

	std::string basedir;
	cv::Mat ICurr, IPrev;
	char format[256] = "images/image_0/%06d.png";

	cv::Mat T = cv::Mat::eye(4, 4, CV_32F);
	cv::Mat_<float> pose(4, 4);
	pose << 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1;
	std::vector<cv::Mat> poses(1, pose.reshape(1, 1));

	cv::Mat_<float> K(3, 3);
	K << 721.5377, 0.0, 609.5593, 0.0, 721.5377, 172.854, 0.0, 0.0, 1.0;

	cv::Rect roi = cv::Rect(520, 225, 200, 100);
	cv::Mat_<float> Warp(3, 3);
	Warp << 1, 0, roi.x, 0, 1, roi.y, 0, 0, 1;

	cv::Mat_<float> coordsys(4, 4);
	coordsys << 0, -1, 0, 0, 0, 0, -1, 0, 1, 0, 0, 0, 0, 0, 0, 1;
	cv::Mat coordsysInv = coordsys.inv();

	ESM esm;
	esm.setHeight(1.65);
	esm.setPitch(0.0);
	esm.setDoF(2);
	esm.setIntrinsic(K);
	esm.setROI(roi);

	ICurr = get_mono_image(format, 0);
	for (int i = 1; !ICurr.empty(); i++) {
		cv::imshow("ESM", ICurr);
		if('q' == (char) cv::waitKey(1)) exit(0);

		if (!IPrev.empty()) {
			bool converged = esm.minSSDSE2Motion(IPrev(roi), ICurr, K, Warp, T);
			pose *= cv::Mat(coordsys*T*coordsysInv).inv();
		}
		IPrev = ICurr;
		ICurr = get_mono_image(format, i);

		list.push_back(pose(cv::Rect(0, 0, 4, 3)).reshape(1, 1));
	}

	write_to_file("esm.txt", list);

	return 0;
}
