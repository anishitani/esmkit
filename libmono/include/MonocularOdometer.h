/**
 * @file MonocularOdometer.h
 * @date 2014.04.24
 * @author André Nishitani <atoshio25@gmail.com>
 */

#ifndef MONOCULARODOMETER_H_
#define MONOCULARODOMETER_H_

#include <cstdlib>

#include <vector>

#include <viso.h>

typedef unsigned char uchar;

/*
 *
 */
class MonocularOdometer: public VisualOdometry {
public:
	// monocular-specific parameters (mandatory: height,pitch)
	struct parameters : public VisualOdometry::parameters {
		double height;           	// camera height above ground (meters)
		double pitch;            	// camera pitch (rad, negative=pointing down)
		int ransac_iters;     		// number of RANSAC iterations
		double inlier_threshold; 	// fundamental matrix inlier threshold
		double motion_threshold; 	// directly return false on small motions
		parameters() {
			height = 1.0;
			pitch = 0.0;
			ransac_iters = 2000;
			inlier_threshold = 0.00001;
			motion_threshold = 100.0;
		}
	};
	typedef struct parameters parameters;

	MonocularOdometer(parameters param);
	virtual ~MonocularOdometer();

	bool process(uchar *I, int* dims, float scale = -1);

private:
	/*
	 * Odômetro monocular e parâmetros relacionados
	 */
	parameters param;
	float scale;
	int dims[3]; ///< Dimensões da imagem: largura, altura e step (= largura para grayscale)

	std::vector<double> estimateMotion(std::vector<Matcher::p_match> p_matched);
	bool normalizeFeaturePoints(std::vector<Matcher::p_match> &p_matched,
			Matrix &Tp, Matrix &Tc);
	void fundamentalMatrix(const std::vector<Matcher::p_match> &p_matched,
			const std::vector<int> &active, Matrix &F);
	void EtoRt(Matrix &E, Matrix &K, std::vector<Matcher::p_match> &p_matched,
			Matrix &X, Matrix &R, Matrix &t);
	int triangulateChieral(std::vector<Matcher::p_match> &p_matched, Matrix &K,
			Matrix &R, Matrix &t, Matrix &X);
	std::vector<int> getInlier(std::vector<Matcher::p_match> &p_matched,
			Matrix &F);

};

#endif /* MONOCULARODOMETER_H_ */
