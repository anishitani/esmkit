/*
 * OdometerInterface.h
 *
 *  Created on: Apr 23, 2014
 *      Author: nishitani
 */

#ifndef BASEODOMETER_H_
#define BASEODOMETER_H_

#include <opencv2/opencv.hpp>

/*
 * Interface para métodos de odometria.
 */
class BaseOdometer {
public:
	virtual ~BaseOdometer(){};

	/*
	 * Processamento:
	 * Onde a estimativa da pose é efetivamente calculada
	 */
	virtual int process(cv::Mat I) = 0;

	cv::Mat getPose(){ return pose; }
	cv::Mat getMotion(){ return motion; }
	cv::Mat getCurrentImage(){ return ICurr; }
	cv::Mat getPreviousImage(){ return IPrev; }

protected:
	cv::Mat IPrev, ICurr; 		///< Imagem no formato do OpenCV
	cv::Mat motion;			///< Tranformação entre dois frames
	cv::Mat pose;			///< Pose da câmera após o pós processamento

	virtual int init_odometer() = 0;

	/*
	 * Pré processamento:
	 * Dependendo do método utilizado pode ser necessário que
	 * sejam realizados processamentos como extração de features,
	 * normalização da imagem, entre outras opções.
	 */
	virtual int pre_process() = 0;

	/*
	 * Pós processamento:
	 * Qualquer processamento necessário após a extração da
	 * pose. Um exemplo é a integração da pose obtida para gerar
	 * a odometria.
	 */
	virtual int pos_process() = 0;

};

#endif /* BASEODOMETER_H_*/
