/**
 * @author André Nishitani <atoshio25@gmail.com>
 * @date 2014.06.02
 * @section DESCRITPION
 */

#include <cstdio>
#include <fstream>

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <MonocularOdometer.h>
#include <esm.h>

cv::Mat get_mono_image(char *format, int i) {
	char filename[256];
	sprintf(filename, format, i);
	return cv::imread(filename, CV_LOAD_IMAGE_GRAYSCALE);
}

void write_to_file(std::string filename, cv::Mat matrix) {
	FILE *file;
	file = fopen(filename.c_str(), "w");
	for (int i = 0; i < matrix.rows; i++) {
		for (int j = 0; j < matrix.cols; j++) {
			fprintf(file, "%f ", matrix.at<float>(i, j));
		}
		fprintf(file, "\n");
	}
	fclose(file);
}

int main(int argc, char **argv) {
	cv::Mat list;

	std::string basedir;
	cv::Mat ICurr, IPrev;
	char format[256] = "images/image_0/%06d.png";

	cv::Mat_<float> pose(4, 4);
	pose << 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1;
	std::vector<cv::Mat> poses(1, pose.reshape(1, 1));

	cv::Mat T;

	cv::Mat_<float> K(3, 3);
	K << 721.5377, 0.0, 609.5593, 0.0, 721.5377, 172.854, 0.0, 0.0, 1.0;

	MonocularOdometer::parameters param;
	param.calib.f = (K.at<float>(0)+K.at<float>(4))/2;
	param.calib.cu = K.at<float>(2);
	param.calib.cv = K.at<float>(5);
	param.height =  1.65;
	param.pitch = 0.0;
	bool converged = false;
	MonocularOdometer odom(param);

	cv::Rect roi = cv::Rect(520, 225, 200, 100);
	cv::Mat_<float> Warp(3, 3);
	Warp << 1, 0, roi.x, 0, 1, roi.y, 0, 0, 1;

	ESM esm;
	esm.setHeight(1.65);
	esm.setPitch(0.0);
	esm.setDoF(2);
	esm.setIntrinsic(K);
	esm.setROI(roi);
	float scale;

	ICurr = get_mono_image(format, 0);
	for (int i = 1; !ICurr.empty(); i++) {

		int dims[3] = { ICurr.cols, ICurr.rows, ICurr.cols };
		cv::imshow("MONOESM", ICurr);
		if ('q' == (char) cv::waitKey(1))
			exit(0);

		if(!IPrev.empty()){
			T = cv::Mat::eye(4,4,CV_32F);
			scale = esm.minSSDSE2Motion(IPrev(roi), ICurr, K, Warp, T) ? cv::norm(T(cv::Rect(3,0,1,3))) : -1;
		}
		else {
			scale = -1;
		}

		if (odom.process(ICurr.data, dims, scale)) {
			double _motion[16];
			odom.getMotion().getData(_motion, 0, 0, 3, 3);
			cv::Mat(4, 4, CV_64F, _motion).convertTo(T,CV_32F);
			pose *= T.inv();
		}
		IPrev = ICurr;
		ICurr = get_mono_image(format, i);

		list.push_back(cv::Mat(pose)(cv::Rect(0, 0, 4, 3)).reshape(1, 1));
	}

	write_to_file("monoesm.txt", list);

	return 0;
}
