#!/bin/bash

# Valores não encontrados retornam null (eu acho)
#shopt -s nullglob

datasets="/home/nishitani/workspace/data/local-dataset/KITTI/odometry"
odometers="$(find ./build -name 'ODOM_*')"
toRemove='./build/ODOM_'

for i in {0..11}
do
	rm "images"
	ln -s $(printf "$datasets/%02d\n" $i) "images"
	
	for odom in ${odometers[@]}
	do
		./$odom
		odom_name=${odom#$toRemove}
		mv "${odom_name,,}.txt" "data/results/${odom_name,,}/data/$(printf "%02d" $i).txt"
	done
done