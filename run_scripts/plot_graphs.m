function plot_graphs(a, b)

	if (a > b)
		return;
	end

	% Diretório contendo os resultados
	result_dir = [getenv("HOME") "/workspace/esmkit/data/results"];
	
	% Diretório contendo o ground truth
	gt_dir = [ getenv("HOME") "/workspace/esmkit/data/odometry/poses" ];
	
	% Odômetros que irão gerar os plots
	odometers = ["esm";"mono";"monoesm"];
	
	% Gera um mapa de cores com a quantidade de 
	color_map = hsv(rows(odometers)+1)
	
	for i = a:b,
		data = load( sprintf("%s/%02d.txt",gt_dir,i) );
		plot(data(:,4),data(:,12),'Color','black');
		
		hold on;
		
		legend("Ground Truth");
				
		for j = 1:rows(odometers),
			data = load (sprintf( "%s/%s/data/%02d.txt", result_dir, strtrim(odometers(j,:)), i));
			plot(data(:,4),data(:,12),'Color',color_map(j,:));
			legend(odometers(j,:));
		end
		
		hold off;
	end

	clear; 
endfunction