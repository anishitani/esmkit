#!/bin/bash

datasets="/home/nishitani/workspace/data/dataset/KITTI/odometry"

odom=$1
sequence=$2	

rm "images"
ln -s $(printf "$datasets/%02d\n" $sequence) "images"
	
./build/"ODOM_"${odom^^}	
mv "${odom}.txt" "data/results/${odom}/data/$(printf "%02d" $sequence).txt"
