/*
 * ESM.h
 *
 *  Created on: Sep 10, 2013
 *      Author: nishitani
 */

#ifndef ESM_H_
#define ESM_H_

#include <cstdio>
#include <vector>

#include <opencv2/opencv.hpp>

#include <math_tools.h>

class ESM
{
public:
	/**
	 * @brief Construtor da classe ESM. Pode ser inicializado com os valores de graus de liberdade e inclinação.
	 *
	 * @param camHeight Parâmetro obrigatório que define a altura da câmera e consequentemente a escala do movimento estimado
	 * @param dof Graus de liberdade do movimento
	 * @param pitch Inclinação da câmera
	 * @param prefix Posição do template na imagem
	 */
	ESM(float camHeight = 1.0, float pitch = 0.0, int dof = 3, cv::Mat prefix =
			cv::Mat::eye(3, 3, CV_32F))
	{
		this->maxIterations = 50;
		this->convCriteria = 1.0e-4;
		this->camHeight = camHeight;
		this->dof = dof;
		this->pitch = pitch;
		createGenerators();
		pitchEffect();
	}

	~ESM()
	{
	}

	bool minSSDSE2Motion(cv::Mat TIRef, cv::Mat ICur, cv::Mat K, cv::Mat G0,
			cv::Mat &T);

	void setHeight(float camHeight)
	{
		this->camHeight = camHeight;
	}
	void setPitch(float pitch)
	{
		this->pitch = pitch;
		pitchEffect();
	}
	void setDoF(float dof)
	{
		this->dof = dof;
	}
	void setMaxIterations(int maxIterations)
	{
		this->maxIterations = maxIterations;
	}
	void setConvergenceCriteria(float convCriteria)
	{
		this->convCriteria = convCriteria;
	}
	void setIntrinsic(cv::Mat intrinsic)
	{
		this->intrinsic = intrinsic;
	}
	void setROI(cv::Rect roi)
	{
		this->roi = roi;
	}
	void setGenerators(std::vector<cv::Mat> A)
	{
		// Cria um grupo com os geradores fornecidos
		this->A.clear();
		this->A.assign(A.begin(), A.end());
	}
	void setVehicleToCameraTransform(cv::Mat Veh2Cam)
	{
		this->Veh2Cam = Veh2Cam;
	}

	cv::Mat getNormalVector()
	{
		return normVec;
	}
	cv::Mat getVehicleToCameraTransform()
	{
		return Veh2Cam;
	}

private:
	int maxIterations;		///< Número máximo de iterações para minimização
	float convCriteria;		///< Variação mínima para considerar a convergência

	float camHeight;///< Altura da câmera. Parâmetro obrigatório do qual se infere a escala do movimento.
	int dof;			///< Graus de liberdade do movimento
	float pitch;		///< Inclinação da câmera
	int tempRows;		///< Número de linhas (altura) do template
	int tempCols;		///< Número de colunas (largura) do template
	cv::Mat TIRef;		///< Template (recorte) da imagem anterior
	cv::Mat ICur;		///< Imagem atual à qual o template será alinhado

	cv::Mat intrinsic;	///< Parâmetros intrínsecos da câmera
	cv::Mat prefix;		///< Posição do template na imagem

	cv::Mat Veh2Cam;	///< Transformação do frame do veículo para a câmera
	cv::Mat normVec;	///< Vetor normal ao plano do chão
	cv::Rect roi;		///< Região de interesse da imagem;

	std::vector<cv::Mat> A; ///< Array das matrizes geradoras da algebra Lie

	/**
	 * @todo Definir previamente os valores de:
	 * 	cv::Mat K;
	 * 	cv::Mat constMat1;
	 * 	cv::Mat constMat2;
	 * onde constMat1 = K*Veh2Cam e constMat2 = Veh2Cam.inv()*K.inv()
	 */

	/**
	 * @brief Cria os geradores da algebra lie.
	 */
	void createGenerators();

	/**
	 * @brief Realiza as alterações necessárias nas informações dependentes da inclinação da câmera
	 */
	void pitchEffect();

	/**
	 * @brief Passo de minimização para o movimento em SE(2)
	 *
	 * @param TIRef Template da imagem de referência (anterior)
	 * @param ICur Imagem atual
	 * @param width Largura da imagem
	 * @param height Altura da imagem
	 * @param K Matriz da câmera
	 * @param norVec Vetor normal ao plano do chão
	 * @param G0 Transformação aplicada ao template da imagem de referência para posicionamento inicial em relação à imagem atual
	 * @param T Transformação sendo atualizada na iteração
	 * @param RMS Erro ao final da iteração
	 * @return 1 em caso de sucesso
	 */
	bool updateSSDSE2Motion(cv::Mat TIRef, cv::Mat ICur, float width,
			float height, cv::Mat K, cv::Mat G0, cv::Mat &T, float &RMS);

	/**
	 * @brief Calculo do jacobiano para o caso de movimento planar
	 *
	 * @param mIx Média dos gradientes em X dos templates das imagens atual e anterior
	 * @param mIy Média dos gradientes em Y dos templates das imagens atual e anterior
	 * @param width Largura da imagem
	 * @param height Altura da imagem
	 * @param K Matriz da câmera
	 * @param norVec Vetor normal ao plano do chão
	 * @param G0 Transformação aplicada ao template da imagem de referência para posicionamento inicial em relação à imagem atual
	 * @param T Transformação sendo atualizada na iteração
	 * @return Jacobiano
	 */
	cv::Mat imgJacSE2planar(cv::Mat mIx, cv::Mat mIy, float width, float height,
			cv::Mat K, cv::Mat G0, cv::Mat &T);
};

#endif /* ESM_H_ */

