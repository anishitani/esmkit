cmake_minimum_required(VERSION 2.8.7)
project ( ODOM_UNSCALED )
 
find_package( OpenCV REQUIRED ) # Busca o pacote OpenCV
 
set (PROJECT_INCLUDE_DIR ${PROJECT_SOURCE_DIR}/include)
set (PROJECT_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/src)
 
set(ODOM_MONO_SOURCES
    ${PROJECT_SOURCE_DIR}/main.cpp  
)
 
include_directories( ${PROJECT_BINARY_DIR} )
include_directories( ${PROJECT_INCLUDE_DIR} )
include_directories( ${MONO_SOURCE_DIR}/include )
include_directories( ${libviso2_SOURCE_DIR}/src )

add_executable( ${PROJECT_NAME} ${ODOM_MONO_SOURCES} )

target_link_libraries( ${PROJECT_NAME} ${OpenCV_LIBS} MONO viso2 )